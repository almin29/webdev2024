<?php
/*
  Exercice de révision :

    Créez un script PHP.
    Votre script reçoit un ensemble de données structuré en tableau (array) et contenant : email, password, ip, timestamp
    Créez 2 fonctions :
 1/ une fonction qui "nettoie" chaque donnée en supprimant les éventuels espaces (avant et après) et les éventuelles balises HTML
 2/ une fonction qui valide l'ensemble des données reçues par le script, afin de :
    - valider l'email (un @ séparant une partie alphanumérique et un nom de domaine)
    - valider le mot de passe (au moins 8 chars, au moins une lettre minuscule, une lettre majuscule, un chiffre et un charactère spécial
    - valider l'adresse ip (4 nombres compris entre 0 et 255 séparés par un point)
    - valider le timestamp en le convertissant en date et en vérifiant qu'il s'agit bien du jour courant.
*/

function cleanData(string $data) :string {
    return strip_tags(trim($data));
}

/**
 * validate form data
 *
 * @param array $data   $_POST or $_GET
 * @return array
 */
function validDataType(array $data) :array {

    foreach ($data as $key => $value) {
        $data[$key] = cleanData($value);
        if ($key == 'email') {
            $data['email'] = filter_var($value, FILTER_VALIDATE_EMAIL);
        } elseif($key == 'password') {
            if (strlen($value) < 8
                || !preg_match("/[\d]/", $value)
                || !preg_match("/[a-z]/", $value)
                || !preg_match("/[A-Z]/", $value)
                || !preg_match("/[0-9]/", $value)
                || !preg_match("/[\W]/", $value)
            ) {
                $data['password'] = false;
            }
        } elseif ($key == 'ip') {
            $data['ip'] = filter_var($value, FILTER_VALIDATE_IP);
        } elseif ($key == 'timestamp') {
            $date = new \DateTime('2024-02-16 12:13');
            $date->setTimestamp($data['timestamp']);
            if ($date != new DateTime()) {
                $data['timestamp'] = false;
            }

        }
    }
    return $data;
}

