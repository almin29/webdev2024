<?php

/**
 * 1e étape :
 *  Créez un script permettant de récupérer les données du formulaire de signup et affichant, dans un tableau HTML, chaque donnée avec son intitulé et sa valeur
 *
 * 2e étape :
 *  Utilisez la fonction validDataType() sur chaque donnée envoyée par le formulaire.
 *
 * 3e étape :
 *  Inclure la date d'inscription
 *  Aborder la superglobale $_SERVER afin de récupérer l'adresse IP
 *
 */